package main

import (
	"context"
	"net/http"
	"strings"

	vision "cloud.google.com/go/vision/apiv1"
	"github.com/andersfylling/disgord"
	"github.com/auttaja/gommand"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var router *gommand.Router
var visionClient *vision.ImageAnnotatorClient
var log = logrus.New()

var imageExtensions = [...]string{"jpg", "jpeg", "png"}

func main() {
	viper.SetConfigName("config")
	viper.SetConfigType("toml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		log.WithError(err).Fatalln("Couldn't read config")
	}

	visionClient, err = vision.NewImageAnnotatorClient(context.Background())
	if err != nil {
		log.WithError(err).Fatalln("Couldn't open vision client")
	}

	router = gommand.NewRouter(&gommand.RouterConfig{
		PrefixCheck: gommand.MultiplePrefixCheckers(
			gommand.StaticPrefix(viper.GetString("bot.prefix")),
			gommand.MentionPrefix,
		),
	})
	client := disgord.New(disgord.Config{
		BotToken: viper.GetString("bot.token"),
		Logger:   log,
	})
	router.Hook(client)

	client.On(disgord.EvtMessageCreate, handleMessageCreate)
	client.StayConnectedUntilInterrupted(context.Background())
}

func handleMessageCreate(s disgord.Session, evt *disgord.MessageCreate) {
	if evt.Message.Author.Bot || len(evt.Message.Attachments) == 0 {
		return
	}

	images := make([]*disgord.Attachment, 0, len(evt.Message.Attachments))
	for _, v := range evt.Message.Attachments {
		filename := strings.ToLower(v.Filename)
		for _, ext := range imageExtensions {
			if strings.HasSuffix(filename, ext) {
				images = append(images, v)
				break
			}
		}
	}
	if len(images) == 0 {
		return
	}

	for _, info := range images {
		res, err := http.Get(info.URL)
		if err != nil {
			log.WithError(err).Errorln("Couldn't fetch image")
			return
		}
		defer res.Body.Close()

		img, err := vision.NewImageFromReader(res.Body)
		if err != nil {
			logrus.WithError(err).Errorln("Couldn't create vision.Image from image response")
			return
		}
		props, err := visionClient.DetectSafeSearch(context.Background(), img, nil)
		if err != nil {
			logrus.WithError(err).Errorln("Couldn't DetectSafeSearch")
		}

		evt.Message.Reply(context.Background(), s, props.String())
	}
}
