module filty

go 1.14

require (
	cloud.google.com/go v0.63.0
	github.com/andersfylling/disgord v0.18.0
	github.com/auttaja/gommand v1.10.1
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.1
	golang.org/x/sys v0.0.0-20200810151505-1b9f1253b3ed // indirect
	google.golang.org/genproto v0.0.0-20200808173500-a06252235341 // indirect
)
